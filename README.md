# UCAP-Python safeguard PoC

Make UCAP-Python safe again!

### How to run (NFS only)

```bash
$ source /acc/local/share/python/acc-py/next/setup.sh
$ acc-py venv venv
$ pip install pip-tools
$ ./run_all.sh
```

### Where is what

- `*.in`: a set of requirements (i.e. a non-digested `requirements.txt`);
- `*.txt`: requirements digested by `pip-compile`, if the compilation did not fail;
- `*.out`: stdout/stderr of `pip-compile`.

### Observations

- May take more time than a standard `pip install`
- We should make sure that `pip==23.2` (`pip==22.*` provides wrong output for `transitive_conflict2.in`)

# Workflows

Possible workflows to protect UCAP-Python dependencies from user interaction.

## Standard `pip` with `requirements.txt`

### `pip install` 

All `pip install` receive the additional parameter `-r requirements.txt`, which always makes sure that UCAP-Python stuff is happy with the dependencies.

### `pip uninstall`

There's no such thing as `-r` for `pip uninstall`, therefore we have to protect dependencies from Java code. The protected dependencies set should be generated with a `pip freeze`.

### Problems

`pip install -r ...` protects UCAP dependencies, but not dependencies of other bundles installed on the same node. Converters from user `a` might fail after user `b` installs a new bundle which requires dependencies on an unsupported version. We could fix this by adding converter bundles to the `requirements.txt`, but this means more code. Moreover we should also distinguish between what is critical code for accelerator stuff (e.g. converter bundles) and simple libraries.

`pip uninstall` has the same problem. And if we want to protect converter bundle dependencies from being mistakenly uninstalled, we will have to do the same as for frozen UCAP-Python dependencies.

### Summary

In general this approach requires:
- lots of manual interaction with `pip`;
- coupling of Java code with `pip` dynamics;
- hardcoding dependencies at deployment time;
- constant exchange of information between `pip` output and Java code (to update the protected set from being `pip uninstall`ed).

## `pip-compile` + `pip-sync`

### `pip install`

1. The new module is appended to `requirements.in` (or replaced if already referenced);
2. `pip-compile`
3. If successful, `pip-sync`.

### `pip uninstall`

1. The module is removed from `requirements.in`;
2. `pip-compile`
3. If successful, `pip-sync`.

### Problems

Might be slower. It's an external project (open source).

### Summary

1. We're guaranteed to receive a fully working environment for all installed moduled (both UCAP and converter bundles)
2. Much less manual work.
