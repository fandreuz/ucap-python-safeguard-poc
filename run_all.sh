for file_in in $(ls *.in); do
	pip-compile $file_in --verbose \
	                     --resolver=backtracking \
			     --max-rounds 10 \
			     --pre \
                             --allow-unsafe \
			     > $(basename $file_in .in).out 2<&1	
done
